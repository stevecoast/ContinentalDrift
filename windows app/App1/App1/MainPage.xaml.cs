﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using Windows.Devices.Geolocation;
using Windows.UI.Core;
using Windows.Networking;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace App1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private Geolocator _geolocator = null;

        public MainPage()
        {
            this.InitializeComponent();

            // don't let screen go to sleep
            Windows.System.Display.DisplayRequest KeepScreenOnRequest = new Windows.System.Display.DisplayRequest();
            KeepScreenOnRequest.RequestActive();

            StartLocation();
        }

        private async void StartLocation()
        {
            var accessStatus = await Geolocator.RequestAccessAsync(); // ask for location access
            switch (accessStatus)
            {
                case GeolocationAccessStatus.Allowed:
                    _geolocator = new Geolocator { ReportInterval = 1000, MovementThreshold = 0};

                    _geolocator.DesiredAccuracy = Windows.Devices.Geolocation.PositionAccuracy.High;

                    // Subscribe to PositionChanged event to get updated tracking positions
                    _geolocator.PositionChanged += OnPositionChanged;
                  
                    break;
            }

        } // StartLocation

        async private void OnPositionChanged(Geolocator sender, PositionChangedEventArgs e)
        {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                UpdateLocationData(e.Position);
            });
        } // OnPositionChanged

        private void UpdateLocationData(Geoposition position)
        {
            if (position == null)
            {
                textLatitude.Text = "No data";
                textLongitude.Text = "No data";
            }
            else
            {
                String lat = position.Coordinate.Point.Position.Latitude.ToString();
                String lng = position.Coordinate.Point.Position.Longitude.ToString();
                String acc = position.Coordinate.Accuracy.ToString();

                textLatitude.Text = lat;
                textLongitude.Text = lng;
                textAccuracy.Text = acc + "m";

                SendData(lat + "," + lng + "," + acc);
            }
        } //UpdateLocationData

        private async void SendData(String data)
        {
            try
            {
                using (StreamSocket socket = new StreamSocket())
                {
                    HostName hostname = new HostName("10.0.1.6");
                    await socket.ConnectAsync(hostname, "12345");
                    DataWriter writer = new DataWriter(socket.OutputStream);
                    writer.WriteString(data);
                    await writer.StoreAsync();
                }
            }
            catch (Exception e)
            {


            }


        } // SendData
    }
}
